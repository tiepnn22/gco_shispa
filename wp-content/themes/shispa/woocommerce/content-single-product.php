<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php
	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	$term_id = $term->term_id;
	$term_name = $term->name;

	//gallery
	$product_id = get_the_ID();
	$product = new WC_product($product_id);
	$p_gallery_image = $product->get_gallery_image_ids();
	
	//price
	$money =  wc_get_product(get_the_ID());
	$price_old = (float)$money->get_regular_price();
	$price = (float)$money->get_sale_price();

	$single_product_pricecity = get_field('single_product_pricecity', $product_id);
	$single_product_provident = get_field('single_product_provident', $product_id);
	$single_product_brand = get_field('single_product_brand', $product_id);
	$single_product_support = get_field('single_product_support', $product_id);

	$single_product_tab_one = get_field('single_product_tab_one', $product_id);
	$single_product_tab_two = get_field('single_product_tab_two', $product_id);
	$single_product_tab_three = get_field('single_product_tab_three', $product_id);
	$single_product_tab_four = get_field('single_product_tab_four', $product_id);
	$single_product_tab_fine = get_field('single_product_tab_fine', $product_id);
?>

<section class="single-product">
    <div class="container">
    	<div class="bao-while">
    		<div class="single-product-content">
    			<div class="row">

					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-gallery">
						<div class="gallery-full slider-for-p">

							<a href="javascript:void(0)">
								<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo getPostImage(get_the_ID(),"full"); ?>')">
							</a>

							<?php if(!empty( $p_gallery_image )) { ?>
							<?php foreach( $p_gallery_image as $p_gallery_image_kq ){ ?>
							<a href="javascript:void(0)">
								<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo wp_get_attachment_url( $p_gallery_image_kq ); ?>')">
							</a>
							<?php } ?>
							<?php } ?>

						</div>
						<div class="gallery-thumbs">
							<ul class="slider-nav-p">

								<?php if(!empty( $p_gallery_image )) { ?>
								<li>
								    <a href="javascript:void(0)">
								    	<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo getPostImage(get_the_ID(),"full"); ?>')">
								    </a>
								</li>
								
								<?php foreach( $p_gallery_image as $p_gallery_image_kq ){ ?>
								<li>
								    <a href="javascript:void(0)">
								    	<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo wp_get_attachment_url( $p_gallery_image_kq ); ?>')">
								    </a>
								</li>
								<?php } ?>
								<?php } ?>

							</ul>
						</div>
					</div>

					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-info">
						
						<h1 class="entry-title">
							<?php the_title(); ?>
						</h1>

						<div class="entry-price">
							<?php get_template_part("resources/views/show-price"); ?>
						</div>

						<div class="entry-meta">
							Giá thị trường: <?php echo $single_product_pricecity; ?> - Tiết kiệm: <?php echo $single_product_provident; ?>
						</div>

						<div class="entry-brand">
							Thương hiệu: <?php echo $single_product_brand; ?>
						</div>

						<div class="entry-desc">
							<?php echo cut_string(get_the_excerpt(),400,'...'); ?>
						</div>

						<div class="entry-button">
				        	<a class="header-address" href="javascript:void(0)">
				        		<i class="fa fa-shopping-bag"></i>
				        		<!-- <span>Mua hàng</span> -->
								<?php
									if(!empty($price_old)) { ?>

                                    <div class="product__control">
										<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
											<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt">
												<?php _e('Mua hàng', 'text_domain'); ?>
											</button>
										</form>
                                    </div>

								<?php } ?>
				        	</a>

				        	<a class="header-calendar" href="<?php echo get_field('socical_chat_fb', 'option'); ?>" target="_blank">
				        		<i class="fa fa-comments"></i><span>TƯ VẤN THÊM</span>
				        	</a>
						</div>

						<div class="entry-hotline">
							Hotline: <span><?php echo str_replace(' ','',get_field('h_phone', 'option'));?></span>
						</div>
					</div>

	    		</div>
    		</div>
        
    	</div>
    </div>
</section>

<?php get_template_part("resources/views/template-related-product"); ?>

<section class="single-product-tab">
    <div class="container">
    	<div class="bao-while">
    		<div class="single-product-tab-content">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#profile_1" role="tab" data-toggle="tab">
                            THÔNG TIN
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#profile_2" role="tab" data-toggle="tab">
                            CÔNG DỤNG 
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#profile_3" role="tab" data-toggle="tab">
                            LỢI ÍCH
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#profile_4" role="tab" data-toggle="tab">
                            CÁCH SỬ DỤNG
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#profile_5" role="tab" data-toggle="tab">
                            LÝ DO NÊN MUA TẠI SHI BEAUTY
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile_1">
						<?php echo wpautop($single_product_tab_one); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile_2">
                        <?php echo wpautop($single_product_tab_two); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile_3">
                        <?php echo wpautop($single_product_tab_three); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile_4">
                        <?php echo wpautop($single_product_tab_four); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile_5">
                        <?php echo wpautop($single_product_tab_fine); ?>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>