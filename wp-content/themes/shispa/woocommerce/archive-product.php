<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
	$terms_info = get_terms( 'product_cat', array(
		'parent'=> 0,
	    'hide_empty' => false
	) );

	//banner
	$page_banner = get_field('page_banner_default', 'option');
	$data_page_banner = array(
		'image_link'     =>    $page_banner,
		'image_alt'    =>    ''
	);
?>

<?php
	get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-product">
    <div class="container">
    	
		<div class="title-section">
			<h1>SẢN PHẨM SỬ DỤNG</h1>
		</div>
		<div class="page-product-content">
            <div class="row">

            	<?php $i=0; foreach ($terms_info as $terms_info_kq) {
            		$term_id = $terms_info_kq->term_id;
            		$term_link = get_term_link(get_term( $term_id ));
            		$term_name = $terms_info_kq->name;
            		$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
            		$term_image = wp_get_attachment_url( $thumbnail_id );

            		if($term_id != 140) {
        		?>
				<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
								<img src="<?php echo asset('images/8x9.png');?>" style="background-image: url('<?php echo $term_image; ?>')" alt="<?php echo $term_name; ?>">
							</a>
							<div class="p-stt">#<?php echo $i; ?></div>
						</figure>
						<div class="info">
							<div class="title">
								<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
									<h3>
										<?php echo $term_name; ?>
									</h3>
								</a>
							</div>
						</div>
					</div>
				</article>
				<?php } $i++; } ?>

			</div>
        </div>

    </div>
</section>

<?php get_footer( 'shop' ); ?>
