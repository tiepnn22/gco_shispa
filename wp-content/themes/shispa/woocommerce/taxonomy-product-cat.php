<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product-cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     4.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header( 'shop' ); ?>

<?php
	$term_info = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$taxonomy_name = $term_info->taxonomy;

	//banner
	$page_banner_check = get_field('page_banner', 'category_'.$term_info->term_id);
	$page_banner = (!empty($page_banner_check)) ? $page_banner_check : get_field('page_banner_default', 'option');
	$data_page_banner = array(
		'image_link'     =>    $page_banner, 
		'image_alt'    =>    $term_info->name
	);

	$term_parent_id = $term_info->parent;
	$term_parent_info = get_term($term_parent_id, $taxonomy_name);

	//Nếu category ko có parent sẽ đọc chính nó, nếu category có parent sẽ đọc category parent
	if($term_parent_id == 0) {
		$term_id = $term_info->term_id;
		$term_name = $term_info->name;
		$term_desc = $term_info->description;
	} else {
		$term_id = $term_parent_id;
		$term_name = $term_parent_info->name;
		$term_desc = $term_parent_info->description;
	}
	
	$term_childs = get_term_children( $term_id, $taxonomy_name );
	$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
	$term_image = wp_get_attachment_url( $thumbnail_id );
?>

<?php
	get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="tax-product">
    <div class="container">
    	<div class="bao-while">
    	
			<div class="title-section">
				<h1>
					<?php echo $term_name; ?>
				</h1>
			</div>
			<div class="tax-product-info">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 tax-product-desc">
						<?php echo wpautop($term_desc); ?>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 tax-product-thumbnail">
						<?php if(!empty($term_image)) { ?>
							<img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
						<?php } ?>
					</div>
				</div>
	        </div>

	        <div class="tax-product-content">
	        	<div class="row">
	        		<aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 sidebar">
						<div id="show_post_category-3" class="widget widget_show_post_category">
							<div class="widget widget-category">
								<div class="category-title">
									<a href="javascript:void(0)">
										<h3>Dòng sản phẩm</h3>
									</a>
								</div>
								<div class="widget-content">
									<ul>

										<?php
											foreach ($term_childs as $term_childs_kq) {
												$term_link = get_term_link(get_term($term_childs_kq));
												$term_name = get_term($term_childs_kq)->name;
										?>
												<li class="<?php if($term_info->term_id == $term_childs_kq){ echo 'active';} ?>" >
													<a href="<?php echo $term_link; ?>">
														<?php echo $term_name; ?>
													</a>
												</li>
										<?php } ?>

									</ul>
								</div>
							</div>
						</div>
	        		</aside>
	        		<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 tax-product-list">
	        			<div class="row">

							<?php
								$query = query_post_by_taxonomy_paged('product', $taxonomy_name, $term_info->term_id, 9);
								if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

			            		$post_title = get_the_title();
			            		$post_link = get_the_permalink();
			            		$post_image = getPostImage(get_the_ID(),"product");
		            		?>
									<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
										<div class="item">
											<figure>
												<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
													<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
												</a>
											</figure>
											<div class="info">
												<div class="title">
													<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
														<h3>
															<?php echo $post_title; ?>
														</h3>
													</a>
												</div>
												<?php get_template_part("resources/views/show-price"); ?>
											</div>
										</div>
									</article>

							<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

	        			</div>

						<?php
						if(wc_get_loop_prop( 'total_pages' ) > 1) {
							$total   = isset( $total ) ? $total : wc_get_loop_prop( 'total_pages' ); //tong so page
							$current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' ); //page hien tai
							$base    = isset( $base ) ? $base : esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ); //link
							$format  = isset( $format ) ? $format : '';

							if ( $total <= 1 ) {
								return;
							}
							?>
							<nav class="navigation">
								<?php
								echo paginate_links(
									apply_filters(
										'woocommerce_pagination_args',
										array(
											'base'      => $base,
											'format'    => $format,
											'add_args'  => false,
											'current'   => max( 1, $current ),
											'total'     => $total,
											'prev_text' => '«',
											'next_text' => '»',
											'type'      => 'list',
											'end_size'  => 3,
											'mid_size'  => 3,
										)
									)
								);
								?>
							</nav>
						<?php } ?>

	        		</div>
	        	</div>
	        </div>

	    </div>
    </div>
</section>

<?php get_footer( 'shop' ); ?>