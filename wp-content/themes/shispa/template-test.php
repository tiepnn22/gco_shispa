<?php
    /*
    Template Name: Template test
    */
?>


<body>

<style type="text/css" media="screen">
	.button_text {
		color: #FFFFFF;
		cursor: pointer;
		display: inline-block;
		font-size: 40px;
		padding: 15px 30px;
		text-align: center;
		text-decoration: none;
		position: absolute; /*them absolute va left o duoi de no chuyen dong ,ko thi no dung im*/
	}
	@keyframes glowing {
		0% { background-color: green; box-shadow: 0 0 3px green; }
		50% { background-color: #0094FF; box-shadow: 0 0 20px #0094FF; }
		100% { background-color: green; box-shadow: 0 0 3px green; }
	}
	.button_text {
			animation: glowing 2500ms infinite;
	}

	
	* {
		margin:0px;
		padding:0px;
	}
	html{
		height: 100%;
	}
	body{
		padding: 0;
		color: black;
		background-color: black;
		height: 100%;
	}

	a{
		color: #990000;
	}

	div#message{
		width:100%;
		margin: 10px auto;
	}

	div#container{
		background-image: url(https://shispa.gcosoftware.vn/wp-content/uploads/2021/11/wiese.jpg);
		background-repeat:no-repeat;
		background-size: 100% 100%;
		-webkit-perspective: 600px;
		-webkit-perspective-origin: 0% 0%;
		-moz-perspective: 600px;
		-moz-perspective-origin: 0% 0%;
		-ms-perspective: 600px;
		-ms-perspective-origin: 0% 0%;
		perspective: 600px;
		perspective-origin: 0% 0%;

		width: 100%;
		height: 100%;
		color: white;
		margin: 0 auto;
	}

	div#butterfly {
		position: absolute;
		-webkit-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5);
		-webkit-transform-origin: 80% 50%;
		-moz-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5);
		-moz-transform-origin: 80% 50%;
		-ms-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5);
		-ms-transform-origin: 80% 50%;
		transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5);
		transform-origin: 80% 50%;

		left: 0px;
		top: 0px;
		width: 366px;
		height: 208px;
		-webkit-transform-style: preserve-3d;
		-webkit-animation-name: butterflyani;
		-webkit-animation-duration: 5s;
		-webkit-animation-iteration-count: infinite;
		-webkit-animation-timing-function: linear;


		-moz-transform-style: preserve-3d;
		-moz-animation-name: butterflyani;
		-moz-animation-duration: 5s;
		-moz-animation-iteration-count: infinite;
		-moz-animation-timing-function: linear;


		-ms-transform-style: preserve-3d;
		-ms-animation-name: butterflyani;
		-ms-animation-duration: 5s;
		-ms-animation-iteration-count: infinite;
		-ms-animation-timing-function: linear;


		transform-style: preserve-3d;
		animation-name: butterflyani;
		animation-duration: 5s;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
	}

	@-webkit-keyframes butterflyani {
		from {
			-webkit-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		25% {
			-webkit-transform: rotate3d(0, 1, 0, -90deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		50% {
			-webkit-transform: rotate3d(0, 1, 0, -180deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		75% {
			-webkit-transform: rotate3d(0, 1, 0, -270deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		to{
			-webkit-transform: rotate3d(0, 1, 0, -360deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
	}

	@-moz-keyframes butterflyani {
		from {
			-moz-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		25% {
			-moz-transform: rotate3d(0, 1, 0, -90deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		50% {
			-moz-transform: rotate3d(0, 1, 0, -180deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		75% {
			-mozmoz-transform: rotate3d(0, 1, 0, -270deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		to{
			-moz-transform: rotate3d(0, 1, 0, -360deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
	}
	@-ms-keyframes butterflyani {
		from {
			-ms-transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		25% {
			-ms-transform: rotate3d(0, 1, 0, -90deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		50% {
			-ms-transform: rotate3d(0, 1, 0, -180deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		75% {
			-ms-transform: rotate3d(0, 1, 0, -270deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		to{
			-ms-transform: rotate3d(0, 1, 0, -360deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
	}
	@-keyframes butterflyani {
		from {
			transform: rotate3d(0, 1, 0, 0deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		25% {
			transform: rotate3d(0, 1, 0, -90deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		50% {
			transform: rotate3d(0, 1, 0, -180deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		75% {
			transform: rotate3d(0, 1, 0, -270deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
		to{
			transform: rotate3d(0, 1, 0, -360deg) scale3d(0.5, 0.5, 0.5) translate3d(-300px, 350px, 0px);
		}
	}
	div#rightwing{
		-webkit-transform: rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg) ;
		-webkit-transform-origin: top right;

		-moz-transform: rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg) ;
		-moz-transform-origin: top right;

		-ms-transform: rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg) ;
		-ms-transform-origin: top right;

		transform: rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg) ;
		transform-origin: top right;
		position: absolute;
		left:178px;
		top:0px;
		width: 178px;
		height: 208px;
		background-image: url(https://shispa.gcosoftware.vn/wp-content/uploads/2021/11/wing.png);
		background-repeat:no-repeat;
		-webkit-animation-name: rightwingani;
		-webkit-animation-duration: 0.2s;
		-webkit-animation-iteration-count: infinite;
		-webkit-animation-timing-function: linear;

		-moz-animation-name: rightwingani;
		-moz-animation-duration: 0.2s;
		-moz-animation-iteration-count: infinite;
		-moz-animation-timing-function: linear;

		-ms-animation-name: rightwingani;
		-ms-animation-duration: 0.2s;
		-ms-animation-iteration-count: infinite;
		-ms-animation-timing-function: linear;

		animation-name: rightwingani;
		animation-duration: 0.2s;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
	}

	@-webkit-keyframes rightwingani {
		from {
			-webkit-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
		50% {
			-webkit-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 100deg);
		}
		to{
			-webkit-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
	}

	@-moz-keyframes rightwingani {
		from {
			-moz-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
		50% {
			-moz-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 100deg);
		}
		to{
			-moz-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
	}
	@-ms-keyframes rightwingani {
		from {
			-ms-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
		50% {
			-ms-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 100deg);
		}
		to{
			-ms-transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
	}
	@-keyframes rightwingani {
		from {
			transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
		50% {
			transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 100deg);
		}
		to{
			transform:rotateX(90deg) translate3d(-178px, 0px, 0px) rotate3d(0, 1, 0, 180deg);
		}
	}
	div#leftwing{
		-webkit-transform: rotateX(90deg) rotate3d(0, 1, 0, 0deg) ;
		position: absolute;
		-webkit-transform-origin: top right;

		-moz-transform: rotateX(90deg) rotate3d(0, 1, 0, 0deg) ;
		position: absolute;
		-moz-transform-origin: top right;

		-ms-transform: rotateX(90deg) rotate3d(0, 1, 0, 0deg) ;
		position: absolute;
		-ms-transform-origin: top right;

		transform: rotateX(90deg) rotate3d(0, 1, 0, 0deg) ;
		position: absolute;
		transform-origin: top right;

		left:0px;
		top:0px;
		width: 178px;
		height: 208px;
		background-image: url(https://shispa.gcosoftware.vn/wp-content/uploads/2021/11/wing.png);
		background-repeat: no-repeat;
		-webkit-animation-name: leftwingani;
		-webkit-animation-duration: 0.2s;
		-webkit-animation-iteration-count: infinite;
		-webkit-animation-timing-function: linear;
		-moz-animation-name: leftwingani;
		-moz-animation-duration: 0.2s;
		-moz-animation-iteration-count: infinite;
		-moz-animation-timing-function: linear;
		-ms-animation-name: leftwingani;
		-ms-animation-duration: 0.2s;
		-ms-animation-iteration-count: infinite;
		-ms-animation-timing-function: linear;
		animation-name: leftwingani;
		animation-duration: 0.2s;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
	}

	@-webkit-keyframes leftwingani {
		from {
			-webkit-transform:rotateX(90deg) rotate3d(0, 1, 0, 0deg);
		}
		50% {
			-webkit-transform:rotateX(90deg) rotate3d(0, 1, 0, 80deg);
		}
		to{
			-webkit-transform:rotateX(90deg) rotate3d(0, 1, 0, 00deg);
		}
	}


	@-moz-keyframes leftwingani {
		from {
			-moz-transform:rotateX(90deg) rotate3d(0, 1, 0, 0deg);
		}
		50% {
			-moz-transform:rotateX(90deg) rotate3d(0, 1, 0, 80deg);
		}
		to{
			-moz-transform:rotateX(90deg) rotate3d(0, 1, 0, 00deg);
		}
	}


	@-ms-keyframes leftwingani {
		from {
			-ms-transform:rotateX(90deg) rotate3d(0, 1, 0, 0deg);
		}
		50% {
			-ms-transform:rotateX(90deg) rotate3d(0, 1, 0, 80deg);
		}
		to{
			-ms-transform:rotateX(90deg) rotate3d(0, 1, 0, 00deg);
		}
	}


	@-keyframes leftwingani {
		from {
			transform:rotateX(90deg) rotate3d(0, 1, 0, 0deg);
		}
		50% {
			transform:rotateX(90deg) rotate3d(0, 1, 0, 80deg);
		}
		to{
			transform:rotateX(90deg) rotate3d(0, 1, 0, 00deg);
		}
	}
	clearfix:after {
	    .content: ".";
	    display: block;
	    height: 0px;
		margin: 0px;
		padding: 0px;
	    clear: both;
		visibility: hidden;
		overflow:hidden;
		font-size: 0px;
	}

	.clearfix {
		display: inline-block;
	}
</style>

	<div id="container">
		<div class="button_text">Tặng Thuỷ ^.^</div>

		<div id="butterfly">
			<div id="leftwing">
			</div>
			<div id="rightwing">
			</div>
		</div>
	</div>
</body>