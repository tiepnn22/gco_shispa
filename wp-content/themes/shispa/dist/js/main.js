$(document).ready(function(){
	console.log('shispa');

    //back to top
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 50) {
            $('header').addClass('stick');
            // $("#back-to-top").fadeIn(300);
        } else {
            $('header').removeClass('stick');
            // $("#back-to-top").fadeOut(300);
        }
    });
    if ($('#back-to-top').length) {
        $("#back-to-top").on('click', function() {
            $('html, body').animate({
                scrollTop: $('html, body').offset().top,
            }, 1000);
        });
    }
    //click readmore faq
    $('.faq-list .faq-list-content .item .info .read-more-section').click(function(){
        $(this).parent().find('.faq-admin-rep').css("max-height", "100%");
        $(this).parent().find('span').css("display", "none");
    });

    //menu
    $('.main-menu').meanmenu({
        meanScreenWidth: "1200",
        meanMenuContainer: ".mobile-menu",
    });

    //teams
    $('.venobox').venobox({
        numeratio  : true,  //phân trang
        infinigall : true, //lặp vô hạn
        share      : ['facebook', 'twitter', 'linkedin', 'pinterest', 'download']
    });
    
    $('.home-banner-content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        pauseOnHover: true,
        autoplay: true
    });
    $('.home-intro-testimonial-content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        pauseOnHover: true
    });
    $('.page-feedback-video').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        pauseOnHover: true
    });
    if ($('.home-partner-content .row article').length > 5) {
        $('.home-partner-content .row').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            pauseOnHover: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }]
        });
    }
    if ($('.home-product-content .row article').length > 3) {
        $('.home-product-content .row').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            pauseOnHover: true,
            autoplay: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }]
        });
    }
    if ($('.related-product-content .row article').length > 4) {
        $('.related-product-content .row').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            pauseOnHover: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 575,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            }]
        });
    }

    var number_product = $('.single-product-content .gallery-thumbs ul li').length;
    if(number_product > 4) {
        $('.slider-for-p').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            asNavFor: '.slider-nav-p'
        });
        $('.slider-nav-p').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for-p',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
    } else {
        $('.slider-for-p').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            // asNavFor: '.slider-nav-p'
        });
        $('.slider-nav-p').slick({
            slidesToShow: + number_product,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for-p',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
        jQuery('.gallery-thumbs').addClass("fix-item-mobile");
    }

    var number_feedback = $('.page-feedback-content .gallery_1 .gallery-thumbs ul li').length;
    if(number_feedback > 4) {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
    } else {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            // asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: + number_feedback,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
        jQuery('.gallery-thumbs').addClass("fix-item-mobile");
    }

    var number_feedback = $('.page-feedback-content .gallery_2 .gallery-thumbs ul li').length;
    if(number_feedback > 4) {
        $('.slider-for_2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            asNavFor: '.slider-nav_2'
        });
        $('.slider-nav_2').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for_2',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
    } else {
        $('.slider-for_2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            arrows: false,
            dots: false,
            fade: false,
            // asNavFor: '.slider-nav_2'
        });
        $('.slider-nav_2').slick({
            slidesToShow: + number_feedback,
            slidesToScroll: 1,
            pauseOnHover: false,
            // speed: 1000,
            autoplaySpeed: 1000,
            asNavFor: '.slider-for_2',
            arrows: true,
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            // autoplay: true
        });
        jQuery('.gallery-thumbs').addClass("fix-item-mobile");
    }
    
});