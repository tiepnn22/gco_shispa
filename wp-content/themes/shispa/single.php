<?php get_header(); ?>

<?php
	$get_category =  get_the_category(get_the_ID());
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
    
    //banner
    $page_banner_check = get_field('page_banner', 'category_'.$cat_id);
    $page_banner = (!empty($page_banner_check)) ? $page_banner_check : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $cat_name
    );
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-single">
    <div class="container">
        <div class="bao-while">

            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="page-single-info">
                <?php echo get_the_date('d/m/Y'); ?> - <span><?php echo $cat_name; ?></span>
            </div>
            <div class="page-single-content">
                <?php echo wpautop( the_content() ); ?>
            </div>
            
        </div>
    </div>
</section>

<?php get_template_part("resources/views/page-ads"); ?>

<?php get_template_part("resources/views/template-related-post"); ?>
    
<?php get_footer(); ?>