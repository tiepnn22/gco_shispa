<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="header">
    <div class="container">
        <div class="header-content">

            <div class="logo">
                <a href="<?php echo get_option('home');?>">
                    <img src="<?php echo get_field('h_logo', 'option'); ?>" alt="<?php echo get_option('blogname'); ?>">
                </a>
            </div>

            <nav class="menu-primary">
                <div class="main-menu">

                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location' => 'primary',
                                'container_class'=>'container_class',
                                'menu_class'=>'menu',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>

                </div>
                <div class="mobile-menu"></div>
            </nav>

            <div class="header-right">
                <a class="header-address" href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>">
                    <i class="fa fa-phone"></i><span><?php echo get_field('h_phone', 'option'); ?></span>
                </a>
                <a class="header-calendar" href="<?php echo get_field('book_calendar_link', 'option'); ?>">
                    <i class="fa fa-calendar-check-o"></i><span><?php echo get_field('book_calendar_title', 'option'); ?></span>
                </a>
            </div>

        </div>
    </div>
</header>

