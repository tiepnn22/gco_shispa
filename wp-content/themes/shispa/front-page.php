<?php get_header(); ?>

<?php
	$home_banner = get_field('home_banner', 'option');

	$home_why_title = get_field('home_why_title', 'option');
	$home_why_content = get_field('home_why_content', 'option');

	$home_service_title = get_field('home_service_title', 'option');
	$home_service_desc = get_field('home_service_desc', 'option');
	$home_service_content = get_field('home_service_content', 'option');

	$home_ads_content = get_field('home_ads_content', 'option');

	$home_product_title = get_field('home_product_title', 'option');
	$home_product_tax = get_field('home_product_tax', 'option');

	$home_intro_video_title = get_field('home_intro_video_title', 'option');
	$home_intro_video_desc = get_field('home_intro_video_desc', 'option');
	$home_intro_video_iframe = get_field('home_intro_video_iframe', 'option');

	$home_intro_testimonial_title = get_field('home_intro_testimonial_title', 'option');
	$home_intro_testimonial_desc = get_field('home_intro_testimonial_desc', 'option');
	$home_intro_testimonial_gallery = get_field('home_intro_testimonial_gallery', 'option');
	$home_intro_testimonial_link = get_field('home_intro_testimonial_link', 'option');

	$home_partner_title = get_field('home_partner_title', 'option');
	$home_partner_image = get_field('home_partner_image', 'option');

	$home_news_title = get_field('home_news_title', 'option');
	$home_news_selectpost = get_field('home_news_selectpost', 'option');
	$home_news_link = get_field('home_news_link', 'option');
?>

<?php if(!empty( $home_banner )) { ?>
<section class="home-banner">
	<div class="home-banner-content">

		<?php
			foreach ($home_banner as $home_banner_kq) {

        		$banner_image = $home_banner_kq['image'];
        		$banner_link = $home_banner_kq['link'];
		?>

		<article class="item">
			<figure>
				<a href="<?php echo $banner_link; ?>">
					<img src="<?php echo asset('images/5x2.png');?>" style="background-image: url('<?php echo $banner_image; ?>')">
				</a>
			</figure>
		</article>
		<?php } ?>
		
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_why_content )) { ?>
<section class="home-why">
    <div class="container">
		<div class="title-section">
			<h2><?php echo $home_why_title; ?></h2>
		</div>

        <div class="home-why-content">
            <div class="row">

				<?php
					foreach ($home_why_content as $home_why_content_kq) {

            		$post_title = $home_why_content_kq['title'];
            		$post_image = $home_why_content_kq['image'];
            		$post_link = $home_why_content_kq['link'];
				?>

				<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo asset('images/9x8.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
							</a>
							<div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
							</div>
						</figure>
					</div>
				</article>

				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_service_content )) { ?>
<section class="home-service">
    <div class="container">
		<div class="title-section">
			<h2><?php echo $home_service_title; ?></h2>
		</div>
		<div class="desc-section">
			<?php echo $home_service_desc; ?>
		</div>

        <div class="home-service-content">
            <div class="row">

				<?php
					foreach ($home_service_content as $home_service_content_kq) {

            		$post_title = $home_service_content_kq['title'];
            		$post_image = $home_service_content_kq['image'];
            		$post_link = $home_service_content_kq['link'];
				?>

				<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
							</a>
						</figure>
						<div class="info">
							<div class="title">
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<h3>
										<?php echo $post_title; ?>
									</h3>
								</a>
							</div>
						</div>
					</div>
				</article>

				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_ads_content )) { ?>
<section class="home-ads">
    <div class="container">
		<div class="home-ads-content">
            
            <div class="row">

            	<?php
            		foreach ($home_ads_content as $home_ads_content_kq) {

					$post_title = $home_ads_content_kq["title"];
        			$post_desc = $home_ads_content_kq["desc"];
            	?>

				<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
					<div class="item">
						<div class="info">
							<div class="title">
								<?php echo $post_title; ?>
							</div>
							<div class="desc">
				                <?php echo $post_desc; ?>
							</div>
						</div>
					</div>
				</article>

				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_product_tax )) { ?>
<section class="home-product">
    <div class="container">
		<div class="title-section">
			<h2><?php echo $home_product_title; ?></h2>
		</div>

		<div class="home-product-content">
            <div class="row">

            	<?php
            		foreach ($home_product_tax as $home_product_tax_select) {
            			foreach ($home_product_tax_select as $home_product_tax_select_item) {
            				$i = 1;
            				foreach ($home_product_tax_select_item as $home_product_tax_select_item_kq) {

	            		$term_id = $home_product_tax_select_item_kq;
	            		$term_link = get_term_link(get_term( $term_id ));
	            		$term_name = get_term( $term_id, 'product_cat' )->name;
	            		$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
	            		$term_image = wp_get_attachment_url( $thumbnail_id );
            	?>

				<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
								<img src="<?php echo asset('images/8x9.png');?>" style="background-image: url('<?php echo $term_image; ?>')" alt="<?php echo $term_name; ?>">
							</a>
							<div class="p-stt">#<?php echo $i; ?></div>
						</figure>
						<div class="info">
							<div class="title">
								<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
									<h3>
										<?php echo $term_name; ?>
									</h3>
								</a>
							</div>
						</div>
					</div>
				</article>

				<?php $i++; } } } ?>

			</div>
			<div class="read-more-section">
				<a href="<?php echo get_post_type_archive_link( 'product' ); ?>">Xem thêm</a>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<section class="home-intro">
    <div class="container">
    	<div class="home-intro-content">
    		<div class="row">

        		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        			<div class="home-intro-video">
						<div class="title-section">
							<h2><?php echo $home_intro_video_title; ?></h2>
						</div>
						<div class="desc-section">
							<?php echo $home_intro_video_desc; ?>
						</div>

						<div class="home-intro-video-content">
							<img src="<?php echo asset('images/4x3.png');?>">
							<?php echo $home_intro_video_iframe; ?>
						</div>
					</div>
        		</div>

        		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        			<div class="home-intro-testimonial">
						<div class="title-section">
							<h2><?php echo $home_intro_testimonial_title; ?></h2>
						</div>
						<div class="desc-section">
							<?php echo $home_intro_testimonial_desc; ?>
						</div>

						<div class="home-intro-testimonial-content">

							<?php foreach ($home_intro_testimonial_gallery as $home_intro_testimonial_gallery_kq) { ?>

							<article class="item">
								<figure>
									<a href="<?php echo $home_intro_testimonial_link; ?>">
										<img src="<?php echo asset('images/4x3.png');?>" style="background-image: url('<?php echo $home_intro_testimonial_gallery_kq; ?>')">
									</a>
								</figure>
							</article>

							<?php } ?>

						</div>
					</div>
        		</div>

    		</div>
    	</div>
	</div>
</section>

<?php if(!empty( $home_partner_image )) { ?>
<section class="home-partner">
	<div class="title-section">
		<h2><?php echo $home_partner_title; ?></h2>
	</div>

    <div class="home-partner-content">
    	<div class="container">
        	<div class="row">

        		<?php foreach ($home_partner_image as $home_partner_image_kq) { ?>

				<article class="item">
					<figure>
						<a href="javascript:void(0)">
							<img src="<?php echo asset('images/5x3.png');?>" style="background-image: url('<?php echo $home_partner_image_kq; ?>')">
						</a>
					</figure>
				</article>

				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_news_selectpost )) { ?>
<section class="home-news">
    <div class="container">
		<div class="title-section">
			<h2><?php echo $home_news_title; ?></h2>
		</div>

		<div class="home-news-content">
            <div class="row">

				<?php
					foreach ($home_news_selectpost as $home_news_selectpost_kq) {

					$post_id = $home_news_selectpost_kq->ID;
            		$post_title = get_the_title($post_id);
            		$post_date = get_the_date('d/m/Y', $post_id);
            		$post_link = get_post_permalink($post_id);
            		$post_image = getPostImage($post_id,"news");
            		$post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
				?>

				<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
							</a>
						</figure>
						<div class="info">
							<div class="date"><?php echo $post_date; ?></div>
							<div class="title">
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<h3>
										<?php echo $post_title; ?>
									</h3>
								</a>
							</div>
						</div>
					</div>
				</article>

				<?php } ?>

			</div>
			<div class="read-more-section">
				<a href="<?php echo $home_news_link; ?>">Xem thêm</a>
			</div>				
		</div>
	</div>
</section>
<?php } ?>

<?php get_footer(); ?>

