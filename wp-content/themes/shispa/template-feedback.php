<?php
    /*
    Template Name: Template Phản hồi
    */
?>

<?php get_header(); ?>

<?php
    $page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    //banner
    $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner = (!empty($page_banner_check[0])) ? $page_banner_check[0] : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $page_name
    );

    $feedback_video = get_field('feedback_video', 'option');
    $feedback_gallery = get_field('feedback_gallery', 'option');
    $feedback_gallery_hot_title = get_field('feedback_gallery_hot_title', 'option');
    $feedback_gallery_hot = get_field('feedback_gallery_hot', 'option');
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-feedback">
    <div class="container">
        <div class="bao-while">
            <div class="fix-container">
            
                <div class="title-section">
                    <h1><?php echo $page_name; ?></h1>
                </div>
                <div class="page-feedback-content">

                    <div class="page-feedback-video">

                        <?php foreach ($feedback_video as $feedback_video_kq) { ?>
                            <div class="item">
                                <img src="<?php echo asset('images/2x1.png');?>">
                                <?php echo $feedback_video_kq['video']; ?>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="page-feedback-gallery gallery_1">
                        <div class="gallery-full slider-for">

                            <?php foreach ($feedback_gallery as $feedback_gallery_kq) { ?>
                                <a href="javascript:void(0)">
                                    <img src="<?php echo asset('images/2x1.png');?>" style="background-image: url('<?php echo $feedback_gallery_kq['image']; ?>')">
                                    <div class="gallery-full-info">
                                        <div class="gallery-full-info-title">
                                            <?php echo $feedback_gallery_kq['title']; ?>
                                        </div>
                                        <div class="gallery-full-info-desc">
                                            <?php echo $feedback_gallery_kq['desc']; ?>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>

                        </div>
                        <div class="gallery-thumbs">
                            <ul class="slider-nav">

                                <?php foreach ($feedback_gallery as $feedback_gallery_kq) { ?>
                                <li>
                                    <a href="javascript:void(0)">
                                        <img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $feedback_gallery_kq['image']; ?>')">
                                    </a>
                                </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                    
                    <div class="title-section">
                        <h2><?php echo $feedback_gallery_hot_title; ?></h2>
                    </div>

                    <div class="page-feedback-gallery gallery_2">
                        <div class="gallery-full slider-for_2">

                            <?php foreach ($feedback_gallery_hot as $feedback_gallery_kq) { ?>
                                <a href="javascript:void(0)">
                                    <img src="<?php echo asset('images/2x1.png');?>" style="background-image: url('<?php echo $feedback_gallery_kq['image']; ?>')">
                                    <div class="gallery-full-info">
                                        <div class="gallery-full-info-title">
                                            <?php echo $feedback_gallery_kq['title']; ?>
                                        </div>
                                        <div class="gallery-full-info-desc">
                                            <?php echo $feedback_gallery_kq['desc']; ?>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>

                        </div>
                        <div class="gallery-thumbs">
                            <ul class="slider-nav_2">

                                <?php foreach ($feedback_gallery_hot as $feedback_gallery_kq) { ?>
                                <li>
                                    <a href="javascript:void(0)">
                                        <img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $feedback_gallery_kq['image']; ?>')">
                                    </a>
                                </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part("resources/views/template-taxservice-relatedpost"); ?>

<?php get_footer(); ?>
