<?php
	/*
	Template Name: Template Hỏi đáp
	*/
?>

<?php get_header(); ?>

<?php
	$page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    //banner
    $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner = (!empty($page_banner_check[0])) ? $page_banner_check[0] : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $page_name
    );
    
    $page_faq_cat = get_field('page_faq_cat', 'option');

    $page_faq_email_nick = get_field('page_faq_email_nick', 'option');
    $page_faq_email_pass = get_field('page_faq_email_pass', 'option');
    $page_faq_email = get_field('page_faq_email', 'option');
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-faq">
    <div class="container">
    	<div class="bao-while">
    		<div class="bao-faq">

				<div class="title-section">
					<h1><?php echo $page_name; ?></h1>
				</div>
				<div class="page-page-content">
		            <?php the_content(); ?>
		        </div>

				<!-- <div class="page-single-meta">
					Để lại câu hỏi theo mẫu dưới đây. Câu hỏi của bạn sẽ được trả lời bơi những bác sĩ giàu kinh nghiệm.
				</div>
				<div class="desc-section">
					Việc đọc trước những câu hỏi đã được trả lời sẽ tiết kiệm thời gian cho bạn.
				</div> -->

				<div class="page-faq-content">
					<?php
						$data_faq = array(
							'page_faq_cat'  => $page_faq_cat,
							'page_faq_email_nick'  => $page_faq_email_nick,
							'page_faq_email_pass'  => $page_faq_email_pass,
							'page_faq_email'  => $page_faq_email
						);
						get_template_part("resources/views/faq",$data_faq);
					?>
		        </div>

	    	</div>
    	</div>
    </div>
</section>

<section class="faq-list">
    <div class="container">
    	<div class="bao-while">
    		<div class="bao-faq">

				<div class="title-section">
					<h1>Câu hỏi thường gặp</h1>
				</div>
				<div class="faq-list-content">

					<?php
						$query = query_post_by_taxonomy_paged('faqs', 'faqs-cat', $page_faq_cat, 3);
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

						$post_id = get_the_ID();
	            		$post_title = get_the_title($post_id);
	            		$post_content = get_the_content($post_id);
	            		$post_date = get_the_date('Y/m/d', $post_id);
	            		$post_link = get_post_permalink($post_id);
	            		$post_image = getPostImage($post_id,"news");
	            		$post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');

	            		$faq_name = get_field('faq_name', $post_id);
	            		$faq_phone = get_field('faq_phone', $post_id);
	            		$faq_email = get_field('faq_email', $post_id);
	            		$faq_title = get_field('faq_title', $post_id);
	            		$faq_faq = get_field('faq_faq', $post_id);
	            		$faq_date = get_field('faq_date', $post_id);
	            		$faq_rep = get_field('faq_rep', $post_id);
            		?>

					<article class="item">
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo asset('images/icon/women.png');?>">
							</a>
						</figure>
						<div class="info">
							<div class="faq-user">
								<div class="faq-user-title">
									<a href="javascript:void(0)">
										<h3>
											<?php echo $faq_title; ?>
										</h3>
									</a>
								</div>
								<div class="faq-user-name">
									Hỏi bởi: <span><?php echo $faq_name; ?></span>
								</div>
								<div class="faq-user-date">
									Đăng ngày: <span><?php echo $faq_date; ?></span>
								</div>
								<div class="faq-user-faq">
									<?php echo $faq_faq; ?>
								</div>
							</div>
							<div class="faq-admin">
								<div class="faq-admin-title">Trả lời:</div>
								<div class="faq-admin-support">
									<figure>
										<a href="javascript:void(0)">
											<img src="<?php echo get_field('h_logo', 'option'); ?>">
										</a>
									</figure>
									<div class="info">
										<div class="faq-admin-rep">
											<?php echo $faq_rep; ?>
										</div><span>...</span>
										<div class="read-more-section">
											<a href="javascript:void(0)">Xem thêm</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</article>

					<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

		        </div>

				<nav class="navigation">
					<?php wp_pagenavi( array( 'query' => $query ) ); ?>
				</nav>

	    	</div>
    	</div>
    </div>
</section>

<?php get_footer(); ?>