<footer class="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 footer-logo">
					<div class="footer-logo-content">
						<div class="f-logo">
							<a href="<?php echo get_option('home');?>">
								<img src="<?php echo get_field('f_logo', 'option'); ?>" alt="<?php echo get_option('blogname'); ?>">
							</a>
						</div>
						<div class="f-socical">
							<a href="<?php echo get_field('link_facebook', 'option'); ?>"><i class="fa fa-facebook"></i></a>
							<a href="<?php echo get_field('link_gmail', 'option'); ?>"><i class="fa fa-google-plus"></i></a>
							<a href="<?php echo get_field('link_youtube', 'option'); ?>"><i class="fa fa-youtube-play"></i></a>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 footer-address">
					<div class="footer-title">
						<h2><?php echo get_field('f_contact_title', 'option'); ?></h2>
					</div>
					<address class="footer-address-content">
			        	<div>
			        		<i class="fa fa-map-marker"></i>
			        		<span>
								<?php echo wpautop(get_field('f_contact_address', 'option')); ?>
			        		</span>
			        	</div>
			        	<div>
			        		<i class="fa fa-phone"></i>
			        		<span><?php echo get_field('f_contact_phone', 'option'); ?></span>
			        	</div>
			        	<div>
			        		<i class="fa fa-envelope-o"></i>
			        		<span><?php echo get_field('f_contact_gmail', 'option'); ?></span>
			        	</div>
					</address>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 footer-map">
					<div class="footer-title">
						<h2><?php echo get_field('f_map_title', 'option'); ?></h2>
					</div>
					<div class="footer-map-content">
						<img src="<?php echo asset('images/3x2.png');?>">
						<?php echo get_field('f_map_iframe', 'option'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<?php echo get_field('f_bottom', 'option'); ?>
		</div>
	</div>
</footer>


<div id="tool__society">
    <div class="tool__item">
        <a href="tel:<?php echo get_field('socical_tel', 'option'); ?>" class="tool__icon tool__icon_tel">
            <img src="<?php echo asset('images/icon/icon-phone.png'); ?>">
        </a>
        <a href="https://zalo.me/<?php echo get_field('socical_zalo', 'option'); ?>" class="tool__icon tool__icon_zalo" target="_blank">
           <img src="<?php echo asset('images/icon/icon-zalo.png'); ?>"> 
        </a>
        <a href="<?php echo get_field('socical_chat_fb', 'option'); ?>" class="tool__icon tool__icon_mes" target="_blank">
            <img src="<?php echo asset('images/icon/icon-mes.png'); ?>">
        </a>

        <a href="javascript:void(0)" id="back-to-top" class="tool__icon tool__icon_back">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
</div>


<?php wp_footer(); ?>

<?php
	$effect_shispa = get_field('effect_shispa', 'option');
	if($effect_shispa == 'effect_no') {
		
	} elseif ($effect_shispa == 'effect_snow') {
		get_template_part("resources/effect/snow-effect");
	} elseif ($effect_shispa == 'effect_blossom') {
		get_template_part("resources/effect/blossom-effect");
	}
?>


</body>
</html>