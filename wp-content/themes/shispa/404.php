<?php get_header(); ?>

<section class="page-content"> 
	<div class="container">
		<style type="text/css">
			body.error404 {
			    background-image: url("<?php echo asset('images/img/bg-product.png');?>");
				background-size: cover;
				-moz-background-size: cover;
				-webkit-background-size: cover;
				-o-background-size: cover;
				-ms-background-size: cover;
			    background-repeat: repeat-x;
			    background-position: top center;
			}
		    .page-404 {
		        min-width: 50%;
		        padding: 0;
		        margin: auto;
		        text-align: center;
		    }
		    .page-404-text-err {
			    font-size: 288px;
			    text-align: center;
		        font-family: MulishBold;
			    display: block;
			    line-height: 1.5em;
			    color: #fff;
		    }
		    .page-404-title{
			    font-size: 64px;
			    font-family: PlayfairDisplayBold;
			    color: #fff;
		        margin: 50px 0;
		    }
		    .page-404-title3 {
		    	font-size: 30px;
		    	color: #fff;
		    	display: block;
		    	text-align: center;
		    }
		    .page-404-title2 {
			    display: inline-block;
			    margin: 50px 0 100px;
			    font-size: 28px;
			    color: #DFA21C;
			    font-family: PlayfairDisplayBold;
			    padding: 20px 40px;
			    background: #fff;
			    text-transform: uppercase;
			    border-radius: 5px;
			    -moz-border-radius: 5px;
			    -webkit-border-radius: 5px;
			    -o-border-radius: 5px;
			    -ms-border-radius: 5px;
			}
		</style>

		<div class="page-404">
	        <span class="page-404-text-err">
	        	404
	        </span>
	        <h1 class="page-404-title">
	        	<?php _e('Không tìm thấy trang !', 'text_domain'); ?>
	        </h1>
	        <span class="page-404-title3">
	        	<?php _e('Trang đã bị xóa hoặc địa chỉ URL không đúng', 'text_domain'); ?>
	        </span>
	        <p>
	        	<a class="page-404-title2" href="<?php echo get_option('home');?>">
	        		<?php _e('Quay về trang chủ', 'text_domain'); ?>
	        	</a>
	        </p>
		</div>
	</div>
</section>

<?php get_footer(); ?>