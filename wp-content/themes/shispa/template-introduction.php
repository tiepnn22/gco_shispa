<?php
    /*
    Template Name: Template Về chúng tôi
    */
?>

<?php get_header(); ?>

<?php
    $page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    //banner
    $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner = (!empty($page_banner_check[0])) ? $page_banner_check[0] : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $page_name
    );
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-page">
    <div class="container">
        <div class="bao-while">

            <div class="title-section">
                <h1><?php echo $page_name; ?></h1>
            </div>
            <div class="page-page-content">
                <?php the_content(); ?>
            </div>

        </div>
    </div>
</section>

<?php get_template_part("resources/views/template-team-doctor"); ?>

<?php get_footer(); ?>
