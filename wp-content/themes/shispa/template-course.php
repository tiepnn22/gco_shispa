<?php
	/*
	Template Name: Template Khoá học
	*/
?>

<?php get_header(); ?>

<?php
	$page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    //banner
    $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner = (!empty($page_banner_check[0])) ? $page_banner_check[0] : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $page_name
    );

    $course_info = get_field('course_info');
    $course_schedule_image = get_field('course_schedule_image');
    $course_schedule_title = get_field('course_schedule_title');
    $course_schedule_content = get_field('course_schedule_content');
    $course_schedule_info = get_field('course_schedule_info');

    $form_course_image = get_field('form_course_image', 'option');
    $form_course_form = get_field('form_course_form', 'option');
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-science">
    <div class="container">
    	<div class="bao-while">

        <div class="module module__page-science">
			<div class="title-section">
				<h1><?php echo $page_name; ?></h1>
			</div>
            <div class="module__content">
                <div class="science">

					<?php
						$i = 1;
						foreach ($course_info as $course_info_kq) {
							$course_info_image = $course_info_kq["image"];
							$course_info_title = $course_info_kq["title"];
							$course_info_content = $course_info_kq["content"];
					?>
	                    <div class="science__group">
	                        <div class="science__item">
	                            <img src="<?php echo $course_info_image; ?>">
	                        </div>
	                        <div class="science__item">
	                            <div class="science__content">
	                                <h3 class="science__title">
	                                    <?php echo $course_info_title; ?>
	                                </h3>
	                                <div class="science__ct">

										<?php
											$j = 1;
											foreach ($course_info_content as $course_info_content_kq) {
												$course_info_content_row = $course_info_content_kq["content_row"];
										?>
		                                    <p class="science__text">
		                                        <span class="science__number">
		                                            <?php
		                                            	if($i!=0&&$i%2==0) {
		                                            		echo '<i class="fa fa-check-circle-o"></i>';
		                                            	} else {
		                                            		echo $j.'.';
		                                            	}
	                                            	?>
		                                        </span>
		                                        <span><?php echo $course_info_content_row; ?></span>
		                                    </p>
										<?php $j++; } ?>

	                                </div>
	                            </div>
	                        </div>
	                    </div>
					<?php $i++; } ?>

                    <div class="science__group">
                        <div class="science__item">
                            <img src="<?php echo $course_schedule_image; ?>">
                        </div>
                        <div class="science__item">
                            <div class="science__content">
                                <h3 class="science__title">
                                    <?php echo $course_schedule_title; ?>
                                </h3>
                                <div class="science__ct">
                                    <div class="science__time">
                                        <?php echo $course_schedule_content; ?>
                                    </div>

									<?php
										foreach ($course_schedule_info as $course_schedule_info_kq) {
										    $course_schedule_info_date = $course_schedule_info_kq["date"];
										    $course_schedule_info_title = $course_schedule_info_kq["title"];
										    $course_schedule_info_address = $course_schedule_info_kq["address"];
										    $course_schedule_info_time = $course_schedule_info_kq["time"];
										    $course_schedule_info_desc = $course_schedule_info_kq["desc"];
									?>
	                                    <div class="group">
	                                        <div class="item">
	                                            <span class="month__day">
                                        			<?php echo date("m", strtotime($course_schedule_info_date)); ?>
                                            	</span>
	                                            <span class="month">
	                                            	Tháng <?php echo date("d", strtotime($course_schedule_info_date)); ?>
	                                            </span>
	                                        </div>
	                                        <div class="item">
	                                            <h3>
	                                                <?php echo $course_schedule_info_title; ?>
	                                            </h3>
	                                            <p>
	                                                <span class="icon"><i class="fa fa-map-marker"></i></span>
	                                                <span class="text"><?php echo $course_schedule_info_address; ?></span>
	                                            </p>
	                                            <p>
	                                                <span class="icon"><i class="fa fa-clock-o"></i></span>
	                                                <span class="text"><?php echo $course_schedule_info_time; ?></span>
	                                            </p>
	                                            <p>
	                                                <span class="icon"><i class="fa fa-play"></i></span>
	                                                <span class="text"><?php echo $course_schedule_info_desc; ?></span>
	                                            </p>
	                                        </div>
	                                    </div>
                                	<?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    	</div>
    </div>
</section>

<?php get_template_part("resources/views/template-team-doctor"); ?>


<?php
	$ads_img = get_field('ads_img', 'option');
	// $ads_link_button = get_field('ads_link_button', 'option');
?>
<section class="page-ads">
    <div class="container">
    	<div class="bao-while">
    		<div class="page-ads-content">

		    	<img src="<?php echo $ads_img; ?>">
		    	<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">Đăng ký ngay</a>

	    	</div>
	    </div>
	</div>
</section>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="fa fa-times"></i>
                    </span>
                </button>
                <div class="modal__form">
                    <div class="item">
                        <div class="frame">
                            <img class="frame--image" src="<?php echo $form_course_image;?>">
                        </div>
                    </div>
                    <div class="item">
                        <?php echo do_shortcode( $form_course_form ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>