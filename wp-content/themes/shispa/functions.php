<?php
require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'shispa' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
include_once get_template_directory(). '/load/Custom_Functions.php';

include_once get_template_directory(). '/load/CustomPost_CustomTaxonomy.php';
// include_once get_template_directory(). '/load/Customize.php';
// include_once get_template_directory(). '/resources/widgets/show-post.php';
include_once get_template_directory(). '/load/wc.php';


/* Create custom post type */
// create_post_type("Portfolio","portfolio","portfolio",array( 'title','editor','thumbnail','excerpt','comments'));
create_post_type("Dịch vụ","services","services",array( 'title','editor','thumbnail','excerpt','comments'));
create_post_type("Đội ngũ","teams","doi-ngu",array( 'title','editor','thumbnail','excerpt','comments'));
create_post_type("Hỏi đáp","faqs","hoi-dap",array( 'title','editor','thumbnail','excerpt','comments'));
/* Create custom taxonomy */
// create_taxonomy_theme("Portfolio Category","portfolio-cat","portfolio-cat","portfolio");
create_taxonomy_theme("Danh mục Dịch vụ","danhmuc-dichvu","services-cat","services");
// create_taxonomy_theme("Danh mục Đội ngũ","danhmuc-doingu","teams-cat","teams");
create_taxonomy_theme("Danh mục Hỏi đáp","danhmuc-hoidap","faqs-cat","faqs");


// Theme option khi dùng Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Tuỳ chỉnh', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Tuỳ chỉnh', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
}


if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_404()) {
            return __( '404 page not found', 'text_domain' );
        }

        return get_the_title();
    }
}


if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image.png') : $img[0];
    }
}


if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


if (!function_exists('getCategories')) {
    function getCategories($tax) {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}terms as terms
                JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id
                WHERE term_tax.taxonomy = '{$tax}'";

        $results = $wpdb->get_results($sql);

        $cates = [];

        foreach ($results as $term) {
            $cates[$term->term_id] = $term->name;
        }

        return $cates;
    }
}


if (!function_exists('format_price')) {
    function format_price($money) {
        $str = "";
        if ($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
        }
        return $str;
    }
}
if (!function_exists('format_price_donvi')) {
    function format_price_donvi($money, $donvi) {
        $str = "";
        if($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
            $str .= $donvi;
            $str = $str;
        }
        return $str;
    }
}
if (!function_exists('show_price_old_price')) {
    function show_price_old_price($old_price, $price, $donvi2) {
        $donvi = !empty($donvi2) ? $donvi2 : '';
        $str = "";
        if($old_price > 0){
            if($price > 0 || $price != null){
                $str1 = format_price_donvi($price, $donvi);
                $str2 = format_price_donvi($old_price, $donvi);
                $str = '<div class="price"><span class="price-news">'.$str1.'</span><span class="price-old">'.$str2.'</span></div>';
            } else {
                $str = '<div class="price"><span class="price-news">'.format_price_donvi($old_price, $donvi).'</span></div>';
            }
        } else{
            // $str = '<a class="contact" href="">Liên hệ</a>';
        }
        return $str;
    }
}

