<?php get_header(); ?>

<?php
	$terms = wp_get_object_terms($post->ID, 'services-cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	$term_id = $term->term_id;
	$term_name = $term->name;

	//banner
    $page_banner_check = get_field('page_banner', 'category_'.$term_id);
    $page_banner = (!empty($page_banner_check)) ? $page_banner_check : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $term_name
    );
    
	$table_service_title = get_field('table_service_title', get_the_ID());
	$table_service_price = get_field('table_service_price', get_the_ID());
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>
    
<section class="page-single">
    <div class="container">
    	<div class="bao-while">

			<div class="title-section">
				<h1><?php the_title(); ?></h1>
			</div>

			<?php if(!empty( get_the_excerpt() )) { ?>
			<div class="page-single-meta">
				<?php the_excerpt(); ?>
			</div>
			<?php } ?>

			<div class="page-single-content">
	        	<?php echo wpautop( the_content() ); ?>
	        </div>

			<div class="page-single-price">
	        	<div class="page-single-price-title">
	        		Bảng giá
	        	</div>
	        	<div class="page-single-price-content">
	        		<div>
	        			<div>Dịch vụ</div>
	        			<div><?php echo $table_service_title; ?></div>
	        		</div>
	        		<div>
	        			<div>Bảng giá dịch vụ</div>
	        			<div><?php echo $table_service_price; ?></div>
	        		</div>
	        	</div>
	        </div>
        
    	</div>
    </div>
</section>

<?php get_template_part("resources/views/page-ads"); ?>

<?php get_template_part("resources/views/social-bar"); ?>

<?php get_template_part("resources/views/template-related-service"); ?>

<?php get_footer(); ?>