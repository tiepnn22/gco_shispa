<?php get_header(); ?>

<?php
	$term_info = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id = $term_info->term_id;
	$term_name = $term_info->name;
	$taxonomy_name = $term_info->taxonomy;
	
	//banner
    $page_banner_check = get_field('page_banner', 'category_'.$term_id);
    $page_banner = (!empty($page_banner_check)) ? $page_banner_check : get_field('page_banner_default', 'option');
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'    =>    $term_name
    );
?>

<?php
    get_template_part("resources/views/page-banner",$data_page_banner);
?>

<section class="page-category">
    <div class="container">
    	
		<div class="title-section">
			<h1><?php echo $term_name; ?></h1>
		</div>
		<div class="page-category-content">
            <div class="row">

				<?php
					$query = query_post_by_taxonomy_paged('services', $taxonomy_name, $term_id, 6);
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

            		$post_title = get_the_title();
            		$post_link = get_the_permalink();
            		$post_image = getPostImage(get_the_ID(),"news");
            		$post_date = get_the_date('d/m/Y');
            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
				?>

					<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
							</a>
						</figure>
							<div class="info">
								<div class="date">
									<?php echo $post_date; ?>
								</div>
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<div class="desc">
					                <?php echo $post_excerpt; ?>
								</div>
								<div class="read-more-section">
									<a href="<?php echo $post_link; ?>">Xem thêm</a>
								</div>
							</div>
						</div>
					</article>

				<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

			</div>
        </div>

		<nav class="navigation">
			<?php wp_pagenavi( array( 'query' => $query ) ); ?>
		</nav>

    </div>
</section>

<?php get_template_part("resources/views/template-taxservice-relatedpost"); ?>

<?php get_footer(); ?>