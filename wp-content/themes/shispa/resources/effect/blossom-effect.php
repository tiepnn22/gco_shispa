    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/resources/effect/effect-plugin.js"></script>

    <style>
        .snow-canvas {
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            pointer-events: none;
            z-index: 9999;
        }
    </style>

    <!-- <canvas class="snow-canvas" speed="1" interaction="false" size="2" count="80" opacity="0.00001" start-color="rgba(253,252,251,1)" end-color="rgba(251,252,253,0.3)" wind-power="0" image="false" width="1366" height="667"></canvas>
    <canvas class="snow-canvas" speed="2" interaction="true" size="6" count="30" start-color="rgba(253,252,251,1)" end-color="rgba(251,252,253,0.3)" opacity="0.00001" wind-power="2" image="false" width="1366" height="667"></canvas> -->
    <canvas class="snow-canvas" speed="3" interaction="true" size="12" count="20" wind-power="-5" image="../../../../../wp-content/themes/shispa/resources/effect/blossom.png" width="1366" height="667"></canvas>

    <script>
        $(".snow-canvas").snow();
    </script>
