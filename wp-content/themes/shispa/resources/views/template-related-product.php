<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => 'product',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 100,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	) );
?>


<aside class="related-product">
	<div class="container">
		<div class="bao-while">

			<div class="related-title">
				<h3>Sản phẩm liên quan</h3>
			</div>
			<div class="related-product-content">
				<div class="row">
					
					<?php
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

	            		$post_title = get_the_title();
	            		$post_link = get_the_permalink();
	            		$post_image = getPostImage(get_the_ID(),"product");
					?>

					<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="item">
							<figure>
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" rel="bookmark">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<?php get_template_part("resources/views/show-price"); ?>
							</div>
						</div>
					</article>

					<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

				</div>
			</div>

		</div>
	</div>
</aside>