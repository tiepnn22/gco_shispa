<?php
	global $data_page_banner;
	
	$image_link = $data_page_banner['image_link'];
	$image_alt = $data_page_banner['image_alt'];
?>

<section class="page-header">
	<div class="container">
		<div class="page-header-content">

			<div class="page-banner">
				<img src="<?php echo $image_link; ?>" alt="<?php echo $image_alt; ?>">
			</div>

		    <div class="breadcrumbs">
		        <?php
		            if(function_exists('bcn_display')) { 
		                echo '<a href="' . site_url() . '">Trang chủ </a> > ';
		                bcn_display(); 
		            }
		        ?>
		    </div>

		</div>
	</div>
</section>