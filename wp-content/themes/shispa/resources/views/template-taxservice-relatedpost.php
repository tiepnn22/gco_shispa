<?php
	$service_related_title = get_field('service_related_title', 'option');
	$service_related_post = get_field('service_related_post', 'option');
?>

<section class="related-post">
	<div class="container">
		<div class="related-title">
			<h3><?php echo $service_related_title; ?></h3>
		</div>
		<div class="related-post-content">
			<div class="row">

				<?php
					foreach ($service_related_post as $service_related_post_kq) {

					$post_id = $service_related_post_kq->ID;
            		$post_title = $service_related_post_kq->post_title;
            		$post_link = get_post_permalink($post_id);
            		$post_image = getPostImage($post_id,"news");
            		$post_excerpt = cut_string($service_related_post_kq->post_excerpt,300,'...');
				?>

				<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
							</a>
						</figure>
						<div class="info">
							<div class="title">
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" rel="bookmark">
									<h4>
										<?php echo $post_title; ?>
									</h4>
								</a>
							</div>
							<div class="desc">
				                <?php echo $post_excerpt; ?>
							</div>
						</div>
					</div>
				</article>
				
				<?php } ?>

			</div>
		</div>
	</div>
</section>