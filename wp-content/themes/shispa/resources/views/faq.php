<form class="form-faqs" id="form-faqs" action="" method="POST">
	<div class="row">
		<div class="col-6">
			<input type="text" name="faq-name" class="faq-name" placeholder="Họ và tên" required>
		</div>
		<div class="col-6">
			<input type="number" name="faq-phone" class="faq-phone" placeholder="Số điện thoại" required>
		</div>
	</div>
	<input type="email" name="faq-email" class="faq-email" placeholder="Email" required>
	<input type="text" name="faq-title" class="faq-title" placeholder="Tiêu đề câu hỏi" required>
	<textarea rows="7" cols="50" name="faq-faq" placeholder="Chi tiết câu hỏi" required></textarea>
	<input type="submit" name="faqSubmit" class="faqSubmit" value="Gửi câu hỏi">
</form>

<?php
	//send mail
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

	global $data_faq;
	// echo '<pre>';
	// var_dump($data_faq);
	// echo '</pre>';
	$page_faq_cat = $data_faq['page_faq_cat'][0];
	$page_faq_email_nick = $data_faq['page_faq_email_nick'];
	$page_faq_email_pass = $data_faq['page_faq_email_pass'];
	$page_faq_email = $data_faq['page_faq_email'];
	$blogname = 'Shispa';

	if(isset($_REQUEST['faqSubmit'])){
		global $wpdb;

		// $wp_posts = $wpdb->prefix."posts";
		$wp_term_relationships = $wpdb->prefix."term_relationships";
		$wp_postmeta = $wpdb->prefix."postmeta";

		$faq_name = $_REQUEST['faq-name'];
		$faq_phone = $_REQUEST['faq-phone'];
		$faq_email = $_REQUEST['faq-email'];
		$faq_title = $_REQUEST['faq-title'];
		$faq_faq = $_REQUEST['faq-faq'];

	    date_default_timezone_set('Asia/Ho_Chi_Minh');//Ho_Chi_Minh là mặc định ở việt nam rồi
	    $date_send = date('d-m-Y H:i:s');

	    //title bài post
	    $title_post = 'Câu hỏi từ - '.$faq_name.' - '.$faq_title;

	    $my_post = array(
	        'post_title' => $title_post,
			'post_content'  => '',
			'post_excerpt' => '',
	        'post_status'   => 'pending',
	        'post_author'   => 1,
	        'post_type' => 'faqs',
	    );
	    $post_id = wp_insert_post($my_post,0);

	    //Thêm vào bảng term_relationships (chửa id category (của all ctp) + all id của post trong category đó)
	    $dataterms = array(
	        'object_id' => $post_id,
	        'term_taxonomy_id' => $page_faq_cat,
	        'term_order' =>0
	    );
	    $wpdb->insert($wp_term_relationships,$dataterms);

	    //Thêm vào bảng post_meta (chứa id Post + id+slug+value Field trong Post đó (acf))
		$post_meta_keys=array("faq_name","faq_phone","faq_email","faq_title","faq_faq","faq_date");
		$post_meta_values=array($faq_name,$faq_phone,$faq_email,$faq_title,$faq_faq,$date_send);
		for($i=0; $i < count($post_meta_values); $i++){
			$post_meta_data = array(
				'meta_id' => null,
				'post_id' => $post_id,
				'meta_key' => $post_meta_keys[$i],
				'meta_value' => $post_meta_values[$i]
			);
			$wpdb->insert($wp_postmeta,$post_meta_data);
		}


		//send mail
	    $mail = new PHPMailer(true);
	    try {
	        $mail->isSMTP();
	        $mail->Host = 'smtp.gmail.com';
	        $mail->SMTPAuth = true;
	        $mail->Username = $page_faq_email_nick;
	        $mail->Password = $page_faq_email_pass;
	        $mail->SMTPSecure = 'tls';
	        $mail->Port = 587;
	        $mail->CharSet = 'UTF-8';
	     
	        //Recipients
	        $mail->setFrom($page_faq_email, $blogname); // Tiêu đề ngắn gọn mail
	        $mail->addAddress($page_faq_email, $blogname); //tới, có thể nhiều mail
	     
	        //Content
	        $mail->isHTML(true);
	        $mail->Subject =  'Câu hỏi từ - '.$faq_name.' - '.$faq_title; // Tiêu đề mail
	        $mail->Body    =  '<br /><b>Họ và tên</b>: '.$faq_name;
	        $mail->Body    .= '<br /><b>Số điện thoại</b>: '.$faq_phone;
	        $mail->Body    .= '<br /><b>Email</b>: '.$faq_email;
	        $mail->Body    .= '<br /><b>Tiêu đề câu hỏi</b>: '.$faq_title;
	        $mail->Body    .= '<br /><b>Chi tiết câu hỏi</b>: '.$faq_faq;
	     
	        $mail->send();

			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){ alert('Câu hỏi đã được gửi đi !'); });
			</script>
			<?php

	    } catch (Exception $e) {
	        // echo 'Lỗi: ', $mail->ErrorInfo;

			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){ alert('Không gửi được câu hỏi !'); });
			</script>
			<?php

	    }
?>
<!-- 		<script type="text/javascript">
			jQuery(document).ready(function(){
				alert('Gửi câu hỏi thành công !');
			});
		</script> -->

		<?php
	}
?>
