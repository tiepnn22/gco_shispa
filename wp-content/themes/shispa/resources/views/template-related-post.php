<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in' => $category_ids,
	    'post__not_in' => array($post->ID),
	    'posts_per_page'=> 4,
	    'ignore_sticky_posts'=>1
    );
    $query = new wp_query( $args );
?>


<aside class="related-post">
    <div class="container">
        <div class="related-title">
            <h3>Tin tức liên quan</h3>
        </div>
        <div class="related-post-content">
            <div class="row">

				<?php
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

            		$post_title = get_the_title();
            		$post_link = get_the_permalink();
            		$post_image = getPostImage(get_the_ID(),"news");
            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
				?>

	                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
	                    <div class="item">
							<figure>
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<img src="<?php echo asset('images/3x2.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
	                        <div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" rel="bookmark">
										<h4>
											<?php echo $post_title; ?>
										<h4>
									</a>
								</div>
	                            <div class="desc">
	                                <?php echo $post_excerpt; ?>
	                            </div>
	                        </div>
	                    </div>
	                </article>

				<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>
        </div>
    </div>
</aside>