<?php
	$ads_img = get_field('ads_img', 'option');
	$ads_link_button = get_field('ads_link_button', 'option');
?>

<section class="page-ads">
    <div class="container">
    	<div class="bao-while">
    		<div class="page-ads-content">

		    	<img src="<?php echo $ads_img; ?>">
		    	<a href="<?php echo $ads_link_button; ?>">Đăng ký ngay</a>

	    	</div>
	    </div>
	</div>
</section>