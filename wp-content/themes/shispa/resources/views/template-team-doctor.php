<?php
    $introduction_title = get_field('team_doctor_title', 'option');
    $introduction_desc = get_field('team_doctor_desc', 'option');
    $introduction_team = get_field('team_doctor_team', 'option');
?>

<section class="team-doctor">
    <div class="container">
        <div class="bao-while">

            <?php if ( is_page_template( 'template-course.php' ) ) { ?>

                <div class="title">
                    <h3><?php echo $introduction_title; ?></h3>
                </div>

            <?php } else { ?>

                <div class="title-section">
                    <h2><?php echo $introduction_title; ?></h2>
                </div>
                <div class="desc-section">
                    <?php echo wpautop($introduction_desc); ?>
                </div>

            <?php } ?>

            <div class="team-doctor-content">
                <div class="row">

                    <?php 
                        $i = 1; foreach ($introduction_team as $introduction_team_kq) {

                        $post_id = $introduction_team_kq->ID;
                        $post_title = $introduction_team_kq->post_title;
                        $post_link = get_post_permalink($post_id);
                        $post_image = getPostImage($post_id,"team");
                        $post_excerpt = $introduction_team_kq->post_excerpt;
                    ?>

                        <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="item venobox" data-gall="myGallery" data-vbtype="inline" href="#inline-content<?php echo $i; ?>">
                                <figure>
                                    <a href="javascript:void(0)" title="<?php echo $post_title; ?>">
                                        <img src="<?php echo asset('images/3x3.png');?>" style="background-image: url('<?php echo $post_image; ?>')" alt="<?php echo $post_title; ?>">
                                    </a>
                                </figure>
                                <div class="info">
                                    <div class="title">
                                        <a href="javascript:void(0)" title="<?php echo $post_title; ?>">
                                            <h3>
                                                <?php echo $post_title; ?>
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="desc">
                                        <?php echo $post_excerpt; ?>
                                    </div>
                                    <div class="read-more-section">
                                        <!-- <a class="venobox" data-gall="myGallery" data-vbtype="inline" href="#inline-content<?php echo $i; ?>"> -->
                                        <a href="javascript:void(0)">
                                            Xem thêm
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </article>

                    <?php $i++; } ?>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="team-doctor-popup" style="display: none;">

    <?php 
        $i = 1; foreach ($introduction_team as $introduction_team_kq) {

        $post_id = $introduction_team_kq->ID;
        $post_title = $introduction_team_kq->post_title;
        $post_link = get_post_permalink($post_id);
        $post_image = getPostImage($post_id,"full");
        $post_excerpt = $introduction_team_kq->post_excerpt;
        $post_content = $introduction_team_kq->post_content;
    ?>
        <div id="inline-content<?php echo $i; ?>">
            <article class="team-doctor-popup-item">
                <figure>
                    <a href="javascript:void(0)">
                        <img src="<?php echo $post_image; ?>">
                    </a>
                </figure>
                <div class="info">
                    <div class="title">
                        <a href="javascript:void(0)">
                            <h3>
                                <?php echo $post_title; ?>
                            </h3>
                        </a>
                    </div>
                    <div class="desc">
                        <?php echo $post_excerpt; ?>
                    </div>
                    <div class="meta">
                        <?php echo wpautop($post_content); ?>
                    </div>
                </div>
            </article>
        </div>
    <?php $i++; } ?>

</section>