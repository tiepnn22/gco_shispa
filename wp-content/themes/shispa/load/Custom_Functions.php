<?php
function theme_Style() {
    wp_register_style( 'tool-style', get_stylesheet_directory_uri() . "/dist/css/tool.min.css",false, 'all' );
    wp_enqueue_style('tool-style');
    wp_register_style( 'main-style', get_stylesheet_directory_uri() . "/dist/css/main.css",false, 'all' );
    wp_enqueue_style('main-style');

	wp_register_script( 'tool-script', get_stylesheet_directory_uri() . "/dist/js/tool.min.js", array('jquery'),false,true );
	wp_enqueue_script('tool-script');
    wp_register_script( 'main-script', get_stylesheet_directory_uri() . "/dist/js/main.js", array('jquery'),false,true );
    wp_enqueue_script('main-script');

    // $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    // $params = array(
    //     'ajax_url' => admin_url('admin-ajax.php', $protocol),
    // );
    // wp_localize_script('template-scripts', 'ajax_obj', $params);
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'theme_Style');


// Remove Version Css Js
function core_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
if (!is_admin()) add_filter( 'style_loader_src', 'core_remove_ver_css_js', 9999 );
if (!is_admin()) add_filter( 'script_loader_src', 'core_remove_ver_css_js', 9999 );


// function add_async_attribute_css($tag, $handle) {
//     $scripts_to_async = array('tool-style', 'main-style');
//     foreach($scripts_to_async as $async_script) {
//         if ($async_script === $handle) {
//             return str_replace(' href', ' async="async" href', $tag);
//         }
//     }
//     return $tag;
// }
// function add_async_attribute_js($tag, $handle) {
//     $scripts_to_async = array('tool-script', 'main-script');
//     foreach($scripts_to_async as $async_script) {
//         if ($async_script === $handle) {
//             return str_replace(' src', ' async="async" src', $tag);
//         }
//     }
//     return $tag;
// }
// if (!is_admin()) add_filter('style_loader_tag', 'add_async_attribute_css', 10, 2);
// if (!is_admin()) add_filter('script_loader_tag', 'add_async_attribute_js', 10, 2);


// Remove JQuery migrate
// function core_remove_jquery_migrate( $scripts ) {
//     if ( !is_admin() && isset( $scripts->registered['jquery'] ) ) {
//         $script = $scripts->registered['jquery'];
//         if ( $script->deps ) { 
//             $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
//         }
//     }
// }
// add_action( 'wp_default_scripts', 'core_remove_jquery_migrate' );


// Disable Embeds
function core_my_deregister_scripts(){
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'core_my_deregister_scripts' );


// Disable Dashicons
function core_my_deregister_styles()    { 
   wp_deregister_style( 'dashicons' ); 
}
if ( !is_admin() && !is_user_logged_in() ) add_action( 'wp_print_styles', 'core_my_deregister_styles', 100 );
// Disable Admin bar
function core_hide_admin_bar_from_front_end(){
    if (is_blog_admin()) {
        return true;
    }
    return false;
}
if ( !is_admin() && !is_user_logged_in() ) add_filter( 'show_admin_bar', 'core_hide_admin_bar_from_front_end' );


// Disable emojis
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }
    return $urls;
}





// Remove admin menu
function core_custom_admin_menu() {
    // remove
    remove_menu_page( 'index.php' );                             // index
    remove_menu_page( 'edit-comments.php' );                     // comments
    remove_menu_page( 'tools.php' );                             // tools
    remove_menu_page( 'edit.php?post_type=acf-field-group' );    // acf field
    // remove_menu_page( 'wpclever' );                              // woo quick-view
    // remove_menu_page( 'woocommerce' );                           // woo chính (đơn hàng, cài đặt, ...)
    remove_menu_page( 'woocommerce-marketing' );                 // woo marketing (tiếp thị)

    // remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );   // tag post
    remove_submenu_page( 'themes.php', 'theme-editor.php' );                // themes
    remove_submenu_page( 'plugins.php', 'plugin-editor.php' );              // plugins
    remove_submenu_page( 'options-general.php', 'options-writing.php' );    // setting
    remove_submenu_page( 'options-general.php', 'options-discussion.php' );
    remove_submenu_page( 'options-general.php', 'options-media.php' );
    remove_submenu_page( 'options-general.php', 'options-privacy.php' );
    remove_submenu_page( 'wpcf7', 'wpcf7-integration' );                    // cf7
    remove_submenu_page( 'contact-form-listing', 'import_cf7_csv' );        // cf7 db
    remove_submenu_page( 'contact-form-listing', 'shortcode' );
    remove_submenu_page( 'contact-form-listing', 'extentions' );
    remove_submenu_page( 'contact-form-listing', 'mounstride-CRM' );
    remove_submenu_page( 'plugins.php', 'remove_taxonomy_base_slug' );      // remove_taxonomy_base_slug
    remove_submenu_page( 'options-general.php', 'breadcrumb-navxt' );       // breadcrumb-navxt

    global $menu;       // Global to get menu array
    global $submenu;    // Global to get submenu array

    // rename
    $menu[10][0] = 'Thư viện ảnh';                      // gallery
    if( $menu[45][0] == 'Advanced CF7 DB') {
        $menu[45][0] = 'Dữ liệu form';                  // cf7 db
    }
    $submenu['themes.php'][5][0] = 'Giao diện';         // submenu theme

    // remove
    $submenu['themes.php'][6][1] = '';                  // submenu customize
    $submenu['edit.php?post_type=product'][16][1] = ''; // woo tag
    $submenu['edit.php?post_type=product'][17][1] = ''; // woo attribute
}
add_action( 'admin_menu', 'core_custom_admin_menu' );


if (!function_exists('theme_Setup')) {
    function theme_Setup()
    {
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'text_domain' ),
        ) );

        add_theme_support( 'woocommerce' );
	    add_theme_support( 'post-thumbnails' );
        add_image_size( 'news', 400, 200, true );
	    add_image_size( 'product', 230, 230, true );
        add_image_size( 'team', 270, 270, true );
    }
    add_action('after_setup_theme', 'theme_Setup');
}


if (!function_exists('theme_Widgets')) {
    function theme_Widgets()
    {
        $sidebars = [
            [
                'name'          => __( 'Vùng quảng cáo', 'text_domain' ),
                'id'            => 'adv-ads',
                'description'   => __( 'Vùng quảng cáo dưới chân trang', 'text_domain' ),
                'before_widget' => '<div id="%1$s" class="widget col-lg-6 col-md-6 col-sm-6 col-xs-6 %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'theme_Widgets');
}


//Shortcode play on Widget
add_filter('widget_text','do_shortcode');
//Use Block Editor default for Post
// add_filter('use_block_editor_for_post', '__return_false');
// Remove issues with prefetching adding extra views
// remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


function query_post_by_custompost($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'showposts'=> $numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_custompost_paged($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'showposts'=> $numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


function query_post_by_category($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat' => $cat_id,
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_category_paged($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat' => $cat_id,
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


function query_post_by_taxonomy($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'tax_query' => array(
                                            array(
                                                    'taxonomy' => $taxonomy_name,
                                                    'field' => 'id',
                                                    'terms' => $term_id,
                                                    'operator'=> 'IN'
                                             )),
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_taxonomy_paged($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'tax_query' => array(
                                            array(
                                                    'taxonomy' => $taxonomy_name,
                                                    'field' => 'id',
                                                    'terms' => $term_id,
                                                    'operator'=> 'IN'
                                             )),
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


function query_page_by_page_parent($page_id){
    $qr =  new WP_Query( array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $page_id,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                 ) );
    return $qr;
}






if (!function_exists('remove_footer_admin')) {
    function remove_footer_admin () {
        echo 'Thiết kế website bởi <a href="" target="_blank">GCO</a>';
    }
    add_filter('admin_footer_text', 'remove_footer_admin');
}


if (!function_exists('remove_core_updates')) {
    function remove_core_updates() {
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');
}


?>