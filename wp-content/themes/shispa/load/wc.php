<?php
// Sửa nút đặt hàng page checkout (ko dung đc icl) (css)
add_filter( 'woocommerce_order_button_text', 'checkout_button_text_translation' );
function checkout_button_text_translation( $button_text ) {
    $button_text = 'Gửi';
    return $button_text;
}


// Sửa thông báo Delete sản phẩm page Cart (ko dung đc icl) (css)
add_filter( 'woocommerce_cart_item_removed_title', 'cart_remove_text_translation', 12, 2);
function cart_remove_text_translation( $message, $cart_item ) {
    $product = wc_get_product( $cart_item['product_id'] );
    if( $product )
        // $message = '<div class="wc_delete_undo">Đã xoá sản phẩm '.$product->get_name().'</div>';
        $message = '<div class="wc_delete_undo">Xóa sản phẩm thành công!</div>';

    return $message;
}
add_filter('gettext', 'cart_undo_text_translation', 35, 3 );
function cart_undo_text_translation( $translation, $text, $domain ) {
    if( $text === 'Undo?' ) {
        $translation = '<div class="wc_undo">Hoàn tác!</div>';
    }
    return $translation;
}


// Sửa thông báo Update sản phẩm page Cart (ko dung đc icl) (css)
add_filter('gettext', 'cart_update_text_translation', 10, 3);
function cart_update_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Cart updated.') {
            $translation = '<div class="wc_updated">Cập nhật giỏ hàng thành công!</div>';
        }
    // }
    return $translation;
}


// Sửa thông báo Giỏ hàng trống (ko dung đc icl) (css)
add_filter('gettext', 'cart_empty_text_translation', 10, 3);
function cart_empty_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Your cart is currently empty.') {
            $translation = 'Giỏ hàng hiện đang trống!';
        }
    // }
    return $translation;
}
add_filter('gettext', 'checkout_cart_empty_text_translation', 10, 3);
function checkout_cart_empty_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Checkout is not available whilst your cart is empty.') {
            $translation = 'Thanh toán không có sẵn trong khi giỏ hàng của bạn trống!';
        }
    // }
    return $translation;
}
add_filter('gettext', 'checkout_empty_text_translation', 10, 3);
function checkout_empty_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Sorry, your session has expired.') {
            $translation = 'Xin lỗi, phiên của bạn đã hết hạn!';
        }
    // }
    return $translation;
}


// Sửa thông báo Add vào giỏ hàng thành công
add_filter('wc_add_to_cart_message', 'add_to_cart_text_translation', 10, 2);
function add_to_cart_text_translation($message, $product_id) {
    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $message = 'Thêm vào giỏ hàng thành công !';
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $message = 'Add to cart successfully !';
    // }
    $message = 'Thêm vào giỏ hàng thành công!';
    return $message;
}


// Sửa thông báo Comment rỗng vì chưa chọn rating
add_filter('gettext', 'comment_form_rating_text_translation', 10, 3);
function comment_form_rating_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Please select a rating') {
            $translation = 'Vui lòng chọn một đánh giá';
        }
    // }
    return $translation;
}


// Sửa thông báo validation error form checkout (ko dung đc icl) (css)
add_action( 'woocommerce_after_checkout_validation', 'checkout_validation_unique_error', 9999, 2 );
function checkout_validation_unique_error( $data, $errors ){
    // Check for any validation errors
    if( ! empty( $errors->get_error_codes() ) ) {

        // Remove all validation errors
        foreach( $errors->get_error_codes() as $code ) {
            $errors->remove( $code );
        }

        // Add a unique custom one
        $checkout_validation = '<div class="wc_field_checkout">Trường được yêu cầu!</div>';
        $errors->add( 'validation', $checkout_validation );
    }
}
// Sửa thông báo validation error Email form checkout (ko dung đc icl) (css)
add_filter('gettext', 'invalid_billing_email_text_translation', 10, 3);
function invalid_billing_email_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Invalid billing email address') {
            $translation = '<div class="wc_invalid_email">Địa chỉ e-mail không hợp lệ!</div>';
        }
    // }
    return $translation;
}


// Sửa thông báo Bank page thankyou (ko dung đc icl) (css)
add_filter('gettext', 'atm_bank_alert_text_translation', 10, 3);
function atm_bank_alert_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Our bank details') {
            $translation = 'Chuyển khoản vào tài khoản ngân hàng dưới đây !';
        }
    // }
    return $translation;
}
// Sửa thông báo name Bank page thankyou (ko dung đc icl) (css)
add_filter('gettext', 'bank_text_translation', 10, 3);
function bank_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Bank') {
            $translation = 'Ngân hàng';
        }
    // }
    return $translation;
}
// Sửa thông báo account Bank page thankyou (ko dung đc icl) (css)
add_filter('gettext', 'atm_bank_account_text_translation', 10, 3);
function atm_bank_account_text_translation($translation, $text, $domain) {
    // if ($domain == 'woocommerce') {
        if ($text == 'Account number') {
            $translation = 'Số tài khoản';
        }
    // }
    return $translation;
}


//Tuỳ chỉnh form checkout
function customCheckoutFields($fields) {
    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';

    $fields['billing']['billing_country']['required'] = false;
    $fields['billing']['billing_city']['required'] = false;
    $fields['billing']['billing_state']['required'] = false;

    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);

    unset($fields['billing']['billing_last_name']);
    // unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['account']['account_password']);


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_first_name']['label'] = __('Họ tên');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_first_name']['label'] = __('Name');
    // }
    $fields['billing']['billing_first_name']['label'] = __('Họ và tên');
    $fields['billing']['billing_first_name']['class'] = ['form-row-wide'];
    // $_POST['billing_first_name'] = '';
    // $fields['billing']['billing_first_name']['default'] = "";


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_address_1']['label'] = __('Address');
    // }
    $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    $fields['billing']['billing_address_1']['class'] = ['form-row-wide'];
    $fields['billing']['billing_address_1']['placeholder'] = '';


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_phone']['label'] = __('Phone');
    // }
    $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    $fields['billing']['billing_phone']['class'] = ['form-row-wide'];


    $fields['billing']['billing_email']['label'] = __('Email');
    $fields['billing']['billing_email']['class'] = ['form-row-wide'];


    //them field
    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $label_address = __('Địa chỉ');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $label_address = __('Address');
    // }
    // $fields['billing']['billing_address_3'] = array(
    //     'type'      => 'text',
    //     'label'     => $label_address,
    //     'class'     => array('form-row-wide'),
    //     'required'  => true
    // );


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['order']['order_comments']['label'] = __('Ghi chú');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['order']['order_comments']['label'] = __('Note');
    // }
    $fields['order']['order_comments']['label'] = __('Ghi chú đơn hàng');
    $fields['order']['order_comments']['class'] = ['form-row-wide'];
    $fields['order']['order_comments']['placeholder'] = '';


    //them field
    // $fields['billing']['billing_message'] = array(
    //     'type'      => 'textarea',
    //     'label'     => __('Nội dung', 'woocommerce'),
    //     'placeholder'   => _x('', 'placeholder', 'woocommerce'),
    //     'class'     => array('form-row-wide'),
    //     'required'  => false,
    //     'clear'     => true,
    //     'label_class' => ''
    // );

    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';
    
    return $fields;
}
add_filter('woocommerce_checkout_fields' , 'customCheckoutFields');


// Do bi ham nao do de len nen khong fix dc require nen anh duy lam the
// add_filter('woocommerce_get_country_locale_default', 'testfield');
// function testfield($data) {
//     $data['city']['label'] = 'Thanh pho';
//     $data['city']['required'] = false;
//     // echo '<pre>';
//     // var_dump($data);
//     // echo '</pre>';

//     return $data;
// }


// Xoá value mặc định của input form checkout, hoặc muốn điền sẵn thông tin đăng nhập của user vào form
add_filter( 'woocommerce_checkout_get_value', 'populating_checkout_fields', 10, 2 );
function populating_checkout_fields ( $value, $input ) {
    $token = ( ! empty( $_GET['token'] ) ) ? $_GET['token'] : '';
    // if( 'testtoken' == $token ) {
        // Define your checkout fields  values below in this array (keep the ones you need)
        $checkout_fields = array(
            'billing_first_name'    => ' ',
            // 'billing_last_name'     => 'Wick',
            // 'billing_company'       => 'Murders & co',
            // 'billing_country'       => 'US',
            'billing_address_1'     => ' ',
            // 'billing_address_2'     => 'Royal suite',
            // 'billing_address_3'          => ' ',
            // 'billing_city'          => 'Los Angeles',
            // 'billing_state'         => 'CA',
            // 'billing_postcode'      => '90102',
            'billing_phone'         => ' ',
            'billing_email'         => ' ',
            // 'shipping_first_name'   => 'John',
            // 'shipping_last_name'    => 'Wick',
            // 'shipping_company'      => 'Murders & co',
            // 'shipping_country'      => 'USA',
            // 'shipping_address_1'    => '7 Random street',
            // 'shipping_address_2'    => 'Royal suite',
            // 'shipping_city'         => 'Los Angeles',
            // 'shipping_state'        => 'California',
            // 'shipping_postcode'     => '90102',
            // 'account_password'       => '',
            'order_comments'        => ' ',
        );
        foreach( $checkout_fields as $key_field => $field_value ){
            if( $input == $key_field && ! empty( $field_value ) ){
                // $value = $field_value;
                $value = '';
            }
        }
    // }
    return $value;
}


// Sửa đơn vị giá mặc định của woocommerce
// add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
// function change_existing_currency_symbol( $currency_symbol, $currency ) {
//     if($currency == 'VND') {
//         if(ICL_LANGUAGE_CODE == 'vi'){
//             $currency_symbol = 'đ';
//         } elseif (ICL_LANGUAGE_CODE == 'en') {
//             $currency_symbol = 'đ';
//         }
//     }
//     return $currency_symbol;
// }


// Thay đổi mũi tên trên breadcrumb của wc
// add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
// function jk_woocommerce_breadcrumbs() {
//     return array(
//             'delimiter'   => '<span class="wc-delimiter"> > </span>',
//             'wrap_before' => '<nav class="breadcrumbs wc-breadcrumbs" itemprop="breadcrumb">',
//             'wrap_after'  => '</nav>',
//             'before'      => '',
//             'after'       => '',
//             'home'        => _x( 'Trang chủ', 'breadcrumb', 'woocommerce' ),
//         );
// }


// Redirect đến trang cart khi add_to_cart thành công
// add_filter('woocommerce_add_to_cart_redirect', 'woocommerce_redirect_after_add_to_cart');
// function woocommerce_redirect_after_add_to_cart($url){
//     $url = get_data_language(get_page_link(34), get_page_link(11));
//     return $url;
// }


// Redirect trang thankyou khi order thành công (ko dung đc icl)
// add_action( 'template_redirect', 'woocommerce_redirect_after_checkout' );
// function woocommerce_redirect_after_checkout() {
//     global $wp;
//     if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {

//         // $redirect_url = get_stylesheet_directory_uri().'/en/product/the-food-one';
//         $redirect_url = get_data_language(get_page_link(577), get_page_link(579));

//         wp_redirect($redirect_url );
//         exit;
//     }
// }


// Cũng là redirect trang thankyou, tại trang thankyou (ko dung đc icl)
// add_action( 'woocommerce_thankyou', 'bbloomer_redirectcustom');
// function bbloomer_redirectcustom( $order_id ){
//     $order = wc_get_order( $order_id );
//     $url = 'http://sangodanang.local/san-pham/san-pham-test';
//     if ( ! $order->has_status( 'failed' ) ) {
//         wp_safe_redirect( $url );
//         exit;
//     }
// }






// Show số lượng sản phẩm đã được bán ra
// function show_count_price_product() {
//     global $post;
//     $count = get_post_meta($post->ID,'total_sales', true);
//     $result_count = sprintf( _n( '%s', '%s', $count, 'wpdocs_textdomain' ), number_format_i18n($count));
//     return $result_count;
// }


// Sửa validate email , ko cần
// add_action('woocommerce_checkout_process', 'bbloomer_matching_email_addresses');
// function bbloomer_matching_email_addresses() { 
//     $email1 = $_POST['billing_email'];
//     if (filter_var($email1, FILTER_VALIDATE_EMAIL)) {}else{
//         wc_add_notice( '<div class="wc_field_email">Địa chỉ Email của bạn không hợp lệ !</div>', 'error' );
//     }
// }


// Sửa nút mua hàng ajax (chưa cần)
// add_filter( 'woocommerce_loop_add_to_cart_link', 'chance_loop_add_to_cart_link' );
// function chance_loop_add_to_cart_link( $output ) {
//     global $product;
//     $test = sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
//         esc_url( $product->add_to_cart_url() ),
//         esc_attr( isset( $quantity ) ? $quantity : 1 ),
//         esc_attr( $product->get_id() ),
//         esc_attr( $product->get_sku() ),
//         esc_attr( isset( $class ) ? $class : 'button' ),
//         'Mua hàng'
//     );
//     return $test;
// }


// Sửa nút mua hàng page single product
// add_filter( 'woocommerce_product_single_add_to_cart_text', 'return_button_add_to_cart' );
// function return_button_add_to_cart( $output ) {
//     $output = 'Mua ngay';
//     return $output;
// }